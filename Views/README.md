Lav så mange af de følgende opgaver som du kan:

1. Login-side med to UITextFields og en UIButton som skal sættes op vha. constraints. Komponenterne skal være centreret i viewet. 
2. Justér views så de er korrekt placeret når keyboard vises.
3. Simpel validering af tekstfelterne. Når inputs er valide enables login-knappen.
3. Når der trykkes på login-knappen præsenterer du en tom view controller modally.
