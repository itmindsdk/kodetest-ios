Lav så mange af de følgende opgaver som du kan:

1. Opsæt table view i storyboard vha. constraints.
2. Custom table view cell med komponenterne: UIImageView og UILabel.
3. Simpel model som du kan bruge til at fylde nogle rows i table view.
4. Segue til en detaljevisning når man trykker på en row. Før noget detalje-data med videre som du kan vise vha. UILabel.
5. Custom table view section header view med komponenterne: UILabel.
6. Expandable table view cell
