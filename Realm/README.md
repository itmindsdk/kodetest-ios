Lav så mange af de følgende opgaver som du kan:

1. Opsæt Realm vha. CocoaPods.
2. Opsæt table view i storyboard vha. constraints.
3. Lav en simpel model som du kan skrive til din In-Memory Realm og bruge til at fylde table view.
4. Tilføj en bar button item (system item: Add) til navigationsbaren.
5. Når der trykkes på Add-knappen skal der tilføjes en instans af den simple model til din Realm.
6. Opdatér table view vha. Realm notifications.
