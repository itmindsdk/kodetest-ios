![logo.png](https://bitbucket.org/repo/peBLyb/images/1663685387-logo.png)

Denne test indeholder fem projekter, som hver indeholder nogle opgaver, vi vil have du skal løse.

Løs opgaverne i rækkefølgen: Views -> TableView -> REST -> Realm -> Animation.

Du skal aflevere en "zipped" version af hele din løsning.

Good luck and happy coding!

Krav: Xcode 8.2.1 og Swift 3

# Hvordan downloader jeg testen?
Du kan downloade projektet ved enten at klone dette git repository
eller ved at trykke "Downloads" i menuen til venstre og
derefter vælge "Download repository".